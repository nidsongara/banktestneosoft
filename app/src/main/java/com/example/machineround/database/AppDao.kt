package com.example.machineround.database

import androidx.room.*

@Dao
interface AppDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(imageData: MutableList<ImageData>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertItem(imageItemData: MutableList<ItemListData>)

    @Query("select * from image_data")
    fun getAllImages(): MutableList<ImageData>

    @Query("select * from itemlistdata  WHERE imageId == :id")
    fun getAllImageItems(id: Int): MutableList<ItemListData>

    @Query("DELETE FROM image_data")
    fun deleteImage()

    @Query("DELETE FROM ItemListData")
    fun deleteListData()
}
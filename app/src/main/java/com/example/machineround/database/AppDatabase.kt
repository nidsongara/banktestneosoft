package com.example.machineround.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.machineround.ui.StaticData

@Database(entities = [ImageData::class, ItemListData::class], version = 1, exportSchema = false)
@TypeConverters(MyConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun appDao(): AppDao

    companion object {
        private var instance: AppDatabase? = null

        @Synchronized
        fun getInstance(ctx: Context): AppDatabase {
            if (instance == null)
                instance = Room.databaseBuilder(
                    ctx.applicationContext, AppDatabase::class.java,
                    "app_database"
                )
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build()

            return instance!!

        }


        public fun populateDatabase(db: AppDatabase) {
            val appDao = db.appDao()
            appDao.deleteImage()
            appDao.deleteListData()
            appDao.insert(StaticData.imageData())
            appDao.insertItem(StaticData.itemData())
        }
    }


}
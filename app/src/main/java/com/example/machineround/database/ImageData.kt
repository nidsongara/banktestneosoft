package com.example.machineround.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "image_data")
data class ImageData(
    @ColumnInfo
    val url: String,
    @ColumnInfo
    @PrimaryKey(autoGenerate = false) val id: Int
)
package com.example.machineround.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    foreignKeys = arrayOf(
        ForeignKey(
            entity = ItemListData::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("imageId"),
            onDelete = ForeignKey.CASCADE
        )
    )
)
data class ItemListData(
    @ColumnInfo
    val itemName: String,
    @ColumnInfo
    val imageId: Int,
    @ColumnInfo
    @PrimaryKey(autoGenerate = false) val id: Int? = null
)
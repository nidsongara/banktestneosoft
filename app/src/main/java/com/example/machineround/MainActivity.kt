package com.example.machineround

import ItemListAdapter
import android.os.Bundle
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.machineround.database.AppDao
import com.example.machineround.database.AppDatabase
import com.example.machineround.database.ImageData
import com.example.machineround.database.ItemListData
import com.example.machineround.databinding.ActivityMainBinding
import com.example.machineround.ui.adapter.ViewPagerAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {
    private lateinit var activityMainBinding: ActivityMainBinding
    private lateinit var dataList: MutableList<ImageData>
    private lateinit var db: AppDao
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityMainBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(activityMainBinding.root)
        initialise()
    }

    //initial work
    private fun initialise() {
        db = AppDatabase.getInstance(this).appDao()
        setData()
        setViewPager()
    }

    //set dummy data for list
    private fun setData() {
        dataList = ArrayList()
        AppDatabase.populateDatabase(AppDatabase.getInstance(this))
        dataList = db.getAllImages()

    }

    //set slider adapter
    private fun setViewPager() {
        if (dataList.size > 0) {
            activityMainBinding.viewPager2.adapter = ViewPagerAdapter(this, dataList)
            setItemAdapter(db.getAllImageItems(dataList.get(0).id))
            TabLayoutMediator(
                activityMainBinding.tabLayout,
                activityMainBinding.viewPager2
            ) { tab, position ->
                activityMainBinding.viewPager2.setCurrentItem(tab.position, true)
            }.attach()
            activityMainBinding.tabLayout.addOnTabSelectedListener(object :
                TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab) {
                    activityMainBinding.viewPager2.setCurrentItem(tab.position)
                    setItemAdapter(db.getAllImageItems(dataList.get(tab.position).id))
                }

                override fun onTabUnselected(tab: TabLayout.Tab) {}
                override fun onTabReselected(tab: TabLayout.Tab) {}
            })
        }
    }

    //set list data
    private fun setItemAdapter(dataLists: MutableList<ItemListData>) {
        val itemListAdapter =
            ItemListAdapter(this, dataLists)
        activityMainBinding.recyclerView.adapter = itemListAdapter
        val layoutManager = LinearLayoutManager(
            this,
            RecyclerView.VERTICAL,
            false
        )
        activityMainBinding.recyclerView.layoutManager = layoutManager

        activityMainBinding.itemSearch.setOnQueryTextListener(object :
            SearchView.OnQueryTextListener,
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                itemListAdapter.getFilter().filter(p0)
                return false

            }
        }
        )
    }


}
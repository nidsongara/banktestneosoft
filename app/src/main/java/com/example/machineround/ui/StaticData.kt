package com.example.machineround.ui

import com.example.machineround.database.ImageData
import com.example.machineround.database.ItemListData

class StaticData {
    companion object {
        fun imageData(): MutableList<ImageData> {
            val url1 =
                "https://cdn.sallysbakingaddiction.com/wp-content/uploads/2017/06/moist-chocolate-cupcakes-5.jpg"
            val url2 =
                "https://natashaskitchen.com/wp-content/uploads/2020/05/Vanilla-Cupcakes-3-728x1092.jpg"
            val url3 =
                "https://www.lifeloveandsugar.com/wp-content/uploads/2021/03/Fresh-Strawberry-Cupcakes3.jpg"
            val dataList: MutableList<ImageData>
            dataList = ArrayList()
            dataList.add(ImageData(url1, 1))
            dataList.add(ImageData(url2, 2))
            dataList.add(ImageData(url3, 3))
            return dataList
        }
        fun itemData(): MutableList<ItemListData> {
                 val dataList: MutableList<ItemListData>
            dataList = ArrayList()
            dataList.add(ItemListData("nidhi", 1))
            dataList.add(ItemListData("pria", 1))
            dataList.add(ItemListData("sonia", 1))
            dataList.add(ItemListData("pooja", 1))
            dataList.add(ItemListData("data1", 1))
            dataList.add(ItemListData("data2", 1))
            dataList.add(ItemListData("data3", 1))
            dataList.add(ItemListData("songara", 1))
            dataList.add(ItemListData("android", 1))

            dataList.add(ItemListData("data2", 2))
            dataList.add(ItemListData("data3", 2))
            dataList.add(ItemListData("songara", 2))
            dataList.add(ItemListData("android", 2))
            dataList.add(ItemListData("nidhi", 2))
            dataList.add(ItemListData("pria", 2))
            dataList.add(ItemListData("sonia", 2))
            dataList.add(ItemListData("pooja", 2))
            dataList.add(ItemListData("data1", 2))


            dataList.add(ItemListData("pria", 3))
            dataList.add(ItemListData("sonia", 3))
            dataList.add(ItemListData("pooja", 3))
            dataList.add(ItemListData("data2", 3))
            dataList.add(ItemListData("data3", 3))
            dataList.add(ItemListData("songara", 3))
            dataList.add(ItemListData("android", 3))
            dataList.add(ItemListData("nidhi", 3))
            dataList.add(ItemListData("data1", 3))
            dataList.add(ItemListData("data4", 3))
            dataList.add(ItemListData("data4", 3))

            return dataList
        }
    }
}
import android.content.Context
import android.os.Build
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import androidx.recyclerview.widget.RecyclerView
import com.example.machineround.R
import com.example.machineround.database.ItemListData
import com.example.machineround.databinding.RowListItemBinding
import java.util.*
import kotlin.collections.ArrayList


class ItemListAdapter(
    val context: Context,
    var dataList: MutableList<ItemListData>,
) : RecyclerView.Adapter<ListItemViewHolder>() {
    var dataFilterList: MutableList<ItemListData>

    init {
        dataFilterList = dataList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListItemViewHolder {
        val InflatedView: View =
            LayoutInflater.from(context).inflate(R.layout.row_list_item, parent, false)

        return ListItemViewHolder(InflatedView)
    }

    override fun getItemCount(): Int {
        return dataFilterList.size
    }

    override fun onBindViewHolder(holder: ListItemViewHolder, position: Int) {
        holder.binding.tvItemName.setText(
            dataFilterList.get(position).itemName
        )

    }

     fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    dataFilterList = dataList
                } else {
                    val resultList = ArrayList<ItemListData>()
                    for (row in dataList) {
                        if (row.itemName.toLowerCase()
                                .contains(charSearch.toLowerCase())
                        ) {
                            resultList.add(row)
                        }
                    }
                    dataFilterList = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = dataFilterList
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                dataFilterList = results?.values as ArrayList<ItemListData>
                notifyDataSetChanged()
            }

        }
    }

}

class ListItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val binding = RowListItemBinding.bind(itemView)
}

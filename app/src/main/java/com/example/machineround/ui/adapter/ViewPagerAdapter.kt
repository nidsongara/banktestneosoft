package com.example.machineround.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.machineround.R
import com.example.machineround.database.ImageData
import com.example.machineround.databinding.RowImagesBinding
import java.util.concurrent.Executors
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Handler
import android.os.Looper

class ViewPagerAdapter(
    val context: Context,
    var dataList: MutableList<ImageData>
) : RecyclerView.Adapter<PagerVH>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PagerVH =
        PagerVH(LayoutInflater.from(parent.context).inflate(R.layout.row_images, parent, false))

    override fun onBindViewHolder(holder: PagerVH, position: Int) {
        // Declaring executor to parse the URL
        val executor = Executors.newSingleThreadExecutor()
        val handler = Handler(Looper.getMainLooper())
        var image: Bitmap? = null
        // Only for Background process (can take time depending on the Internet speed)
        executor.execute {
            // Tries to get the image and post it in the ImageView
            // with the help of Handler
            try {
                val `in` = java.net.URL(dataList.get(position).url).openStream()
                image = BitmapFactory.decodeStream(`in`)
                handler.post {
                    holder.binding.imageViewMain.setImageBitmap(image)
                }
            }
            // If the URL doesnot point to
            // image or any other kind of failure
            catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun getItemCount(): Int {
        return dataList.size
    }
}

class PagerVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val binding = RowImagesBinding.bind(itemView)
}